import Vue from 'vue';
import VueRouter from 'vue-router';

//Pages
import Home from '@/pages/home/Index.vue';
import Game from '@/pages/game/Index.vue';
import FormPage from '@/pages/form/Index.vue';
import Tests from '@/pages/tests/Index.vue';

Vue.use(VueRouter);

//Listes des routes
const routes = [
  {
    path: '/',
    name: 'home',
    component: Home
  },
  {
    path: '/game/:id',
    name: 'game',
    component: Game
  },
  {
    path: '/form',
    name: 'form',
    component: FormPage
  },
  {
    path: '/category',
    name: 'tests',
    component: Tests
  },
  {
    path: '/category/:id',
    name: 'category',
    component: Tests
  }
];

const router = new VueRouter({
  routes,
  mode: 'history'
});

export default router;